const { App } = require('@slack/bolt');
require('dotenv').config()

const app = new App({
  signingSecret: process.env.SIGNING_SECRET,
  token: process.env.TOKEN,
  socketMode: true,
  appToken: process.env.APP_TOKEN
});

/* Add functionality here */

(async () => {
  // Start the app
  await app.start(process.env.PORT);

  console.log('⚡️ Bolt app is running!');

  app.message('New patient', async ({ message, say }) => {
    // say() sends a message to the channel where the event was triggered
    console.log(message);
    await say('new patient is received!');
  });
})();